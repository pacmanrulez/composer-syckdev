<?php

namespace Symbiont\Syckdev\Composer;

use Composer\Command\BaseCommand;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Symbiont\Dipendency\Pendency;

use Symbiont\Syckdev\SyckdevPlugin;

abstract class SyckdevBaseCommand extends BaseCommand {

    protected bool $quiet = false;

    protected function execute(InputInterface $input, OutputInterface $output): int {
        Pendency::bind('input', $input);
        Pendency::bind('output', $output);

        return static::FAILURE;
    }

    protected function setCommandName(string $name) {
        $this->setName(SyckdevPlugin::getCommandPrefixed($name));
    }

}