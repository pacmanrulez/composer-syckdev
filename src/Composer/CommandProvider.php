<?php

namespace Symbiont\Syckdev\Composer;

use Composer\Plugin\Capability\CommandProvider as CommandProviderCapability;
use Symbiont\Syckdev\Commands\StatusCommand;

class CommandProvider implements CommandProviderCapability
{
    public function getCommands()
    {
        return [
            new StatusCommand
        ];
    }
}