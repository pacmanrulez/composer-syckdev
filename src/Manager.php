<?php

namespace Symbiont\Syckdev;

use Composer\InstalledVersions;
use Composer\Package\CompletePackage;
use Composer\Package\Link;
use Composer\Repository\InstalledRepository;
use Composer\Repository\RootPackageRepository;
use Symbiont\Dipendency\Pendency;

use Symbiont\Syckdev\Concerns\DealsWithComposerJson;
use Symbiont\Syckdev\Concerns\UsesConfig;
use Symbiont\Syckdev\Concerns\DealsWithPaths;

class Manager {

    use UsesConfig;

    protected array $available = [];

    use DealsWithComposerJson,
        UsesConfig,
        DealsWithPaths;

    public function __construct() {
        $this->initializePackages();
    }

    public static function isSyckdevConfigured() {
        if(file_exists(Config::FILE_SYCKDEV_CONFIG) &&
            $config = Pendency::get('config')) {


                return $config->hasPath();
        }

        return false;
    }

    protected function initializePackages(): void {
        $vendor_path = $this->getComposerVendorPath();
        $symlink_path = $this->getSymlinkPath();

        foreach(array_intersect(
            array_map(function($directory) {
                return basename($directory);
            }, glob($this->asPath([$vendor_path, '/*']), GLOB_ONLYDIR)),
            array_map(function($directory) {
                return basename($directory);
            }, glob($this->asPath([$symlink_path, '/*']), GLOB_ONLYDIR)))
                as $namespace) {

            // get of all available packages, only those who are actually symlink-able
            foreach(glob($symlink_path . '/' . $namespace . '/*', GLOB_ONLYDIR) as $package_name ) {
                $package = "{$namespace}/" . basename($package_name);

                if(is_dir($vendor_path . '/' . $package)) {
                    $this->available[] = new Package($package);
                }
            }
        }

        foreach($this->getPackageAliases() as $package => $alias) {
            // @todo: should be an if and let the package check itself?
            if(is_dir($this->asPath([$vendor_path, $package]))) {
                $this->available[] = new Package($package);
            }
        }
    }

    public function reload(): void {
        $this->available = [];
        $this->initializePackages();
    }

    /**
     * Return packages that are available in general to be symlinked
     * @return array
     */
    public function getAvailablePackages(): array {
        return $this->available;
    }

    /**
     * Return of all available packages only those who are actually required
     * @return array
     */
    public function getRequiredPackages(): array {
        return array_values(array_filter($this->available, function(Package $package) {
            return $package->isRequired();
        }));
    }

    /**
     * Return packages
     * @return array
     */
    public function getDependentPackages(): array {
        return array_values(array_filter($this->available, function(Package $package) {
            return ! $package->isRequired();
        }));
    }

    /**
     * Short incomplete version of `composer why namespace/package`
     * @param string $package
     * @return false|string
     */
    public function whyIsPackageInstalled(string $package): false|string {
        $results = $this->getInstalledRepository()->getDependents($package);

        if(count($results) === 0 ||
           count($results[0]) === 0 ||
            ! ($target = $results[0][0]) instanceof CompletePackage) {
                return false;
        }

        return "{$target->getPrettyName()} ({$target->getPrettyVersion()})";
    }

    public function findPackage(string $package) {
        return $this->getInstalledRepository()->findPackage($package, '*');
    }

    public function getInstalledRepository(): InstalledRepository {
        $composer = Pendency::get('composer');

        return new InstalledRepository([
            new RootPackageRepository(clone $composer->getPackage()),
            $composer->getRepositoryManager()->getLocalRepository()
        ]);
    }

}