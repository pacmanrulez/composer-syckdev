<?php

namespace Symbiont\Syckdev\Concerns;

use Exception;
use Symbiont\Config\Contracts\Configurable;
use Symbiont\Dipendency\Pendency;
use Symbiont\Syckdev\Config;

trait UsesConfig {

    protected ?Configurable $config = null;

    /**
     * Alias
     * @return Config
     */
    public function getConfig(): Config {
        if(! $this->config) {
            $this->config = Pendency::get('config');
        }

        return $this->config;
    }

    /**
     * The path where all packages to be symlinked are stored
     * @return ?string
     * @throws Exception
     */
    public function getSymlinkPath(): ?string {
        return $this->getConfig()->getPath();
    }

    /**
     * Get packages to be symlinked in the vendor directory
     * @return array
     * @throws Exception
     */
    public function getPackages(): array {
        return $this->getConfig()->getPackages();
    }

    /**
     * Get aliased packages
     * @return array
     */
    public function getPackageAliases(): array {
        return $this->getConfig()->getPackageAliases();
    }

}