<?php

namespace Symbiont\Syckdev\Concerns;

trait DealsWithComposerJson {

    protected function getComposerJsonfile(): string {
        return file_get_contents('composer.json');
    }

    /**
     * Get required packages from composer json
     * @return array
     */
    public function getRequiredPackages(): array {
        $composer_file = json_decode($this->getComposerJsonfile(), true);
        if(!array_key_exists('require', $composer_file)) {
            return [];
        }

        return array_keys($composer_file['require']);
    }

    public function isPackageRequired($package) {
        return in_array($package, $this->getRequiredPackages());
    }

}