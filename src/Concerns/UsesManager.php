<?php

namespace Symbiont\Syckdev\Concerns;

use Symbiont\Dipendency\Pendency;
use Symbiont\Syckdev\Config;
use Symbiont\Syckdev\Manager;

trait UsesManager {

    use DealsWithPaths;

    /**
     * @return Config
     */
    public function getManager(): Manager {
        return Pendency::get('manager');
    }

}