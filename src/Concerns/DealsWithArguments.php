<?php

namespace Symbiont\Syckdev\Concerns;

use Symfony\Component\Console\Input\InputInterface;

trait DealsWithArguments {

    /**
     * Argument without name in path
     * @param $arg
     * @return string|null
     */
    public function cleanArgument($arg) {
        if(strpos($arg, '=') === false) {
            return $arg;
        }

        return explode('=', strtolower($arg))[1] ?? null;
    }

    /**
     * Get clean argument
     * @param $name
     * @param InputInterface $input
     * @return string|null
     */
    public function getArgument($name, InputInterface $input) {
        return $this->cleanArgument($input->getArgument($name));
    }

}