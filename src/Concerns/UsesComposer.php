<?php

namespace Symbiont\Syckdev\Concerns;

use Symbiont\Dipendency\Pendency;

trait UsesComposer {

    public function getComposerInstance(): object {
        return Pendency::get('composer');
    }

    public function getComposerVendorPath(): string {
        return $this->getComposerInstance()->getConfig()->get('vendor') ?? './vendor';
    }

}