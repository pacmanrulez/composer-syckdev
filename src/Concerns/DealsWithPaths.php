<?php

namespace Symbiont\Syckdev\Concerns;

trait DealsWithPaths {

    use UsesComposer,
        UsesConfig;

    /**
     * If paths are in given $save_path.
     *
     * @param string|array $paths
     * @param bool $throw
     * @return true|string
     * @throws \Exception
     */
    protected function areSavePaths(string|array $paths, ?string $save_path = null, bool $throw = false): true|string {
        $save_path = $save_path ?? getcwd();

        if(is_string($paths)) {
            $paths = [$paths];
        }

        foreach($paths as $path) {
            if(is_string(($faulty = $this->isSavePath($path, $save_path, $throw)))) {
                return $faulty;
            }
        }

        return true;
    }

    /**
     * If a path is in given $save_path, if so, returns true, else returns the faulty path
     * or if $throw is set to true, an exception will be thrown
     *
     * @param string $path
     * @param string|null $save_path
     * @param bool $throw
     * @return string|true
     * @throws \Exception
     */
    protected function isSavePath(string $path, ?string $save_path = null, bool $throw = false) {
        if(str_starts_with(trim($path), trim($save_path ?? getcwd()))) {
            return true;
        }

        if($throw) {
            throw new \Exception(sprintf('Path %s not in current working directory', $path));
        }

        return $path;
    }

    /**
     * @param string $path
     * @return string
     */
    public function finishPath(string $path) {
        $path = $this->clearPath($path);
        return $path.'/';
    }

    public function clearPath(string $path, bool $both = false) {
        $path = rtrim($path, '/');
        return $both ?
            ltrim($path, '/') :
            $path;
    }

    public function asPath(array $parts) {
        $parts = array_filter($parts);
        foreach($parts as $index => $part) {
            $parts[$index] = $this->clearPath($part);
            if($index > 0) {
                $parts[$index] = $this->clearPath($part, true);
            }
        }
        return implode('/', $parts);
    }

    public function asAbsolutePath(array $parts) {
        $cwd = getcwd();

        foreach($parts as $index => $part) {
            if(str_starts_with($part, './')) {
                $parts[$index] = str_replace('./', '/', $part);
            }
        }

        array_unshift($parts, $cwd);
        return $this->asPath($parts);
    }

    public function asVendorPath(string $name, ?string $path = null) {
        return $this->asPath([$path ?? $this->getComposerVendorPath(), $name]);
    }

    public function asSymlinkPath(string $name, ?string $path = null) {
        return $this->asPath([$path ?? $this->getSymlinkPath(), $this->getRealPackageName($name)]);
    }

    public function getRealPackageName(string $name) {
        return ($this->getConfig())->getPackageFromAlias($name);
    }

}