<?php

namespace Symbiont\Syckdev;

use Composer\Composer;
use Composer\DependencyResolver\Operation\InstallOperation;
use Composer\DependencyResolver\Operation\UpdateOperation;
use Composer\EventDispatcher\EventSubscriberInterface;
use Composer\Installer\PackageEvent;
use Composer\Installer\PackageEvents;
use Composer\IO\IOInterface;
use Composer\Plugin\CommandEvent;
use Composer\Plugin\PluginInterface;
use Composer\Plugin\Capable;
use Composer\Plugin\Capability\CommandProvider as CommandProviderCapability;

use Symbiont\Dipendency\Pendency;

use Symbiont\Syckdev\Composer\CommandProvider;

class SyckdevPlugin implements PluginInterface, Capable, EventSubscriberInterface {

    const CMD_SHORTNAME = 'sd';

    public static $file_root = '';

    public static function getCommandPrefixed(string $command) {
        return self::CMD_SHORTNAME .':'.$command;
    }

    public function activate(Composer $composer, IOInterface $io) {
        Pendency::bind('composer', $composer);
        Pendency::bind('io', $io);
        // @todo: fix creating a new config/manager instance on every call
        Pendency::bind('config', function() { return new Config; });
        Pendency::bind('manager', function() { return new Manager; });
    }

    public function deactivate(Composer $composer, IOInterface $io) { }
    public function uninstall(Composer $composer, IOInterface $io) { }

    public function getCapabilities() {
        return [
            CommandProviderCapability::class => CommandProvider::class,
        ];
    }

    public static function getSubscribedEvents() {
        return [
            PackageEvents::PRE_PACKAGE_UPDATE => [['prePackageUpdate']],
            PackageEvents::POST_PACKAGE_UPDATE => [['postPackageUpdate']],

            PackageEvents::PRE_PACKAGE_INSTALL => [['prePackageInstall']],
            PackageEvents::POST_PACKAGE_INSTALL => [['postPackageInstall']],
            'command' => [['commandCmd']]
        ];
    }

    public function prePackageUpdate(PackageEvent $event) {
        if(! Manager::isSyckdevConfigured()) {
            return;
        }

        /**
         * @var UpdateOperation $operation
         */
        $operation = $event->getOperation();
        $current = $operation->getInitialPackage();

        $package = new Package($current->getName());
        if($package->isActive()) {
            $package->revert();
        }
    }

    public function postPackageUpdate(PackageEvent $event) {
        if(! Manager::isSyckdevConfigured()) {
            return;
        }

        /**
         * @var UpdateOperation $operation
         */
        $operation = $event->getOperation();
        $current = $operation->getTargetPackage();

        /**
         * @var Config $config
         */
        $config = Pendency::get('config');

        $package = new Package($current->getName());
        if($config->packageInList($package->getName()) && $package->activateable()) {
            $package->activate();
        }
    }

    public function prePackageInstall(PackageEvent $event) {
        if(! Manager::isSyckdevConfigured()) {
            return;
        }

        /**
         * @var InstallOperation $operation;
         */
        $operation = $event->getOperation();
        $current = $operation->getPackage();

        // var_dump(__FUNCTION__);
        // var_dump($current->getName());
    }

    public function postPackageInstall(PackageEvent $event) {
        if(! Manager::isSyckdevConfigured()) {
            return;
        }

        /**
         * @var InstallOperation $operation;
         */
        $operation = $event->getOperation();
        $current = $operation->getPackage();

        // var_dump(__FUNCTION__);
        // var_dump(get_Class($current));
    }

    public function commandCmd(CommandEvent $event): void {
        switch($event->getCommandName()) {
            case 'install': break;
            case 'update':
                // var_dump('update');
                break;
            case 'dumpautoload':
            case 'dump-autoload': break;
        }
    }

}