<?php

namespace Symbiont\Syckdev;

use Composer\Package\PackageInterface;
use Symbiont\Dipendency\Pendency;
use Symbiont\Syckdev\Concerns\DealsWithComposerJson;
use Symbiont\Syckdev\Concerns\DealsWithPaths;
use Symbiont\Syckdev\Concerns\UsesComposer;
use Symbiont\Syckdev\Concerns\UsesConfig;

class Package {

    use UsesConfig,
        UsesComposer,
        DealsWithPaths,
        DealsWithComposerJson;

    protected string $name;

    protected string $package_namespace;
    protected string $package_name;

    protected string $path;
    protected string $path_symlink;
    protected string $path_vendor;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;
    const STATUS_INVALID = 4;

    const STATUS_UNKNOWN = 128;

    protected int $status;

    protected bool $aliased = false;
    protected ?string $alias = null;

    protected bool $required;

    public function __construct(string $name, ?string $path = null) {
        $this->setName($name);
        $this->setPath($path ?? $this->getConfig()->getPath());

        $this->checkAlias($name);
        $this->checkStatus();
        $this->checkRequired();
    }

    protected function setName(string $name): void {
        $this->name = $name;
        list($this->package_namespace, $this->package_name) = explode('/', $name);
    }

    protected function setPath(string $path): void {
        $this->path = $path;

        $this->path_symlink = $this->asSymlinkPath($this->name);
        $this->path_vendor = $this->asVendorPath($this->name);
    }

    protected function checkAlias(string $name): void {
        if(($config = $this->getConfig())->hasPackageAlias($name)) {
            $this->aliased = true;
            $this->alias = $config->getPackageFromAlias($name);
        }
    }

    protected function checkStatus(): void {
        $this->status = $this->getStatus();
    }

    protected function checkRequired(): void {
        $this->required = $this->isPackageRequired($this->name);
    }

    public function getName() {
        return $this->name;
    }

    public function getPackageNamespace() {
        return $this->package_namespace;
    }

    public function getPackageName() {
        return $this->package_name;
    }

    public function getTempPackageVendorName() {
        return ".{$this->getPackageName()}.syckdev";
    }

    public function getTempPackageVendorPath() {
        return $this->asPath([$this->getComposerVendorPath(), $this->getPackageNamespace(), $this->getTempPackageVendorName()]);
    }

    public function getPackageVendorPath() {
        return $this->path_vendor;
    }

    public function getPackageSymlinkPath() {
        return $this->path_symlink;
    }

    public function getStatus(): int {
        if($this->isInvalid()) {
            return static::STATUS_INVALID;
        }
        if($this->isActive()) {
            return static::STATUS_ACTIVE;
        }
        if($this->isInactive()) {
            return static::STATUS_INACTIVE;
        }

        return static::STATUS_UNKNOWN;
    }

    public function getReadableStatus(bool $table = false) {
        switch($this->status) {
            case static::STATUS_ACTIVE:
                    return ! $table ? 'active' : "<info>+ active</info>";
                break;
            case static::STATUS_INACTIVE:
                    return ! $table ? 'inactive' : "<comment>- inactive</comment>";
                break;
            case static::STATUS_INVALID:
                    return ! $table ? 'invalid' : "<error>?</error> invalid";
                break;
            case static::STATUS_UNKNOWN;
            default:
                    return ! $table ? 'unknown' : "<error>! unknown</error>";
                break;
        }
    }

    public function getReadableTableStatus(): string {
        return $this->getReadableStatus(true);
    }

    // determine the conditions if a package is currently activated
    public function isActive(): bool {
        if(is_dir($this->getTempPackageVendorPath()) &&
           is_link($vendor_path = $this->getPackageVendorPath()) &&
           readlink($vendor_path) === $this->getPackageSymlinkPath()) {
                return true;
        }

        return false;
    }

    // determine the conditions if a package is currently not activated
    public function isInactive(): bool {
        if(is_dir($this->getPackageVendorPath()) &&
         ! is_link($this->getPackageVendorPath()) ) {
            return true;
        }

        return false;
    }

    public function isInvalid(): bool {
        return ! $this->isComposerPackage();
    }

    // determine the conditions if a package can be activated
    public function activateable(): bool {
        // if current vendor package path is a directory
        if(is_dir($this->getPackageVendorPath()) &&
            // if symlink package path exists
           is_dir($symlink = $this->getPackageSymlinkPath())) {

            return $this->isComposerPackage();
        }

        return false;
    }

    // activate a package
    public function activate(): bool {
        $temp_vendor_path = $this->getTempPackageVendorPath();
        $vendor_path = $this->getPackageVendorPath();

        if(is_string($this->areSavePaths(
            [$temp_vendor_path, $vendor_path],
            $this->getComposerVendorPath()
        ))) {
            return false;
        }

        if($this->isActive()) {
            if(! ($config = $this->getConfig())->packageInList($this->getName())) {
                $config->addPackage($this->getName());
            }
            return true;
        }

        if(! $this->activateable()) {
            throw new \Exception('Unable to activate package ' . $this->name);
        }

        if(is_dir($temp_vendor_path)) {
            $temp_vendor_path_renamed = "$temp_vendor_path." . time();
            rename($temp_vendor_path, $temp_vendor_path_renamed);
        }

        // rename current vendor package path
        if(rename($vendor_path, $temp_vendor_path)) {
            if(symlink($this->getPackageSymlinkPath(), $vendor_path)) {
                $this->status = static::STATUS_ACTIVE;

                $this->getConfig()->addPackage($this->getName());

                return true;
            }

            // revert
            if(is_dir($temp_vendor_path)) {
                rename($temp_vendor_path, $vendor_path);
            }

            return false;
        }

        return false;
    }

    // deactivate a package
    public function deactivate(): bool {
        if($this->isInactive()) {
            if(($config = $this->getConfig())->packageInList($this->getName())) {
                $config->removePackage($this->getName());
            }

            return true;
        }

        if(! $this->revertSymlink()) {
            return false;
        }

        if($this->revertTempVendorDirectory()) {
            $this->status = static::STATUS_INACTIVE;

            $this->getConfig()->removePackage($this->getName());

            return true;
        }

        return false;
    }

    protected function revertTempVendorDirectory(): bool {
        $vendor_path = $this->getPackageVendorPath();
        $temp_vendor_path = $this->getTempPackageVendorPath();

        if(is_string($this->areSavePaths([$vendor_path, $temp_vendor_path], $this->getComposerVendorPath()))) {
            return false;
        }

        if(is_dir($vendor_path)) {
            return false;
        }

        if(is_dir($temp_vendor_path)) {
            return rename($this->getTempPackageVendorPath(), $vendor_path);
        }

        return false;
    }

    protected function cleanupTempVendorDirectories(): void {
        // @todo: implement save and fast method to remove temp vendor directories
    }

    protected function revertSymlink(): bool {
        if(is_link($vendor = $this->getPackageVendorPath())) {
            return unlink($vendor);
        }

        return false;
    }

    /**
     * Revert a package without removing it from config
     * @return void
     */
    public function revert(): bool {
        if($this->revertSymlink()) {
            return $this->revertTempVendorDirectory();
        }

        return false;
    }

    public function isComposerPackage(?string $path = null): bool {
        return file_exists($this->asPath([$path ?? $this->getPackageSymlinkPath(), 'composer.json']));
    }

    public function isRequired() {
        return $this->required;
    }

    public function isInstalled() {
        return is_dir($this->getPackageVendorPath());
    }

    public function isAliased() {
        return $this->aliased;
    }

    public function isAliasValid(): bool {
        if($this->isAliased()) {
            return is_dir($this->getPackageVendorPath()) && is_dir($this->getPackageSymlinkPath());
        }

        return false;
    }

    public function getAlias() {
        return $this->alias;
    }

    public function isGitVersioned(string $path) {
        return is_dir($path) && is_dir($this->asPath([$path, '/.git']));
    }

    /**
     * Returns latest tag or hash. False if no git available.
     * @param string|null $path
     * @return string
     */
    public function getLatestGitVersion(?string $path = null): false|string {
        if(! $this->isGitVersioned($path)) {
            return false;
        }

        $path = $path ?? $this->getPackageSymlinkPath();

        // find main/master branch
        foreach([
            $this->asPath([$path, '/.git/refs/heads/main']),
            $this->asPath([$path, '/.git/refs/heads/master']),
        ] as $file) {
            if(file_exists($file)) {
                $hash_head = trim(file_get_contents($file));
            }
        }
        foreach (array_reverse(glob($this->asPath([$path, '/.git/refs/tags/*']))) as $file) {
            $hash_tag = trim(file_get_contents($file));

            if ($hash_head === $hash_tag) {
                return basename($file);
            }
        }

        if(! isset($hash_head)) {
            return '-';
        }

        return substr($hash_head, 0, 8);
    }

    public function getComposerInstalledVersion(): ?string {
        $manager = Pendency::get('manager');
        $package = $manager->findPackage($this->getName());
        if($package instanceof PackageInterface) {
            return $package->getPrettyVersion();
        }

        return null;
    }

    public function getLocalInDevelopmentVersion(): ?string {
        if(($version = $this->getLatestGitVersion($this->getPackageSymlinkPath())) !== false) {
            return $version;
        }

        return null;
    }

    public function asArray(): array {
        return [
            'name' => $this->getName(),
            'aliased' => ($this->isAliased()),
            'alias' => $this->getAlias(),
            'installed' => $this->getComposerInstalledVersion() ?? false,
            'local' => $this->getLocalInDevelopmentVersion() ?? false,
            'status' => $this->getReadableStatus(),
            'vendor' => ($vendor = $this->getPackageVendorPath()),
            'vendor-exists' => (is_dir($vendor)),
            'symlink' => ($symlink = $this->getPackageSymlinkPath()),
            'symlink-exists' => (is_dir($symlink)),
            'composer-package' => $this->isComposerPackage()
        ];
    }

    public function analyze(bool $styled = true): array {
        $values = $this->asArray();
        $results = [];
        $expected = [
            'name' => function($value) {
                return is_string($value) && str_contains($value, '/');
            },
            'aliased' => function($value) {
                return is_bool($value);
            },
            'alias' => function($value) {
                if(is_null($value)) {
                    return true;
                }
                elseif(is_string($value) && $value && str_contains($value, '/')) {
                    return true;
                }

                return false;
            },
            'installed' => function($value) {
                return str_contains($value, '.');
            },
            'local' => function($value) {
                if(is_string($value)) {
                    return str_contains($value, '.');
                }

                return false;
            },
            'status' => function($value) {
                return $value !== static::STATUS_INVALID && $value !== static::STATUS_UNKNOWN;
            },
            'vendor' => function($value) {
                return is_string($value) && str_contains($value, 'vendor');
            },
            'vendor-exists' => function($value) {
                return $value === true;
            },
            'symlink' => function($value) {
                return is_string($value);
            },
            'symlink-exists' => function($value) {
                return $value === true;
            },
            'composer-package' => function($value) {
                return $value === true;
            },
            'finding' => function($value) {
                return $value === 'all ok';
            }
        ];

        $results['finding'] = [];

        if($values['symlink'] === false || $values['symlink-exists'] === false) {
            $results['finding'][] = "Symlink does not seem be a valid directory\n";
        }
        if($values['composer-package'] === false) {
            $results['finding'][] = "Given (symlink) package does not seem to represent a composer package.\n";
        }
        if($values['alias'] === false) {
            $results['finding'][] = "Alias does not seem to contain a valid package string.\n";
        }
        if($values['local'] === false) {
            $fault = "Unable to determine local version";
            $causes = [];

            if($values['composer-package'] === false) {
                $causes[] = "no composer package";
            }
            if($values['alias'] ===  false) {
                $causes[] = "invalid alias";
            }

            if($causes) {
                $fault .= " (" . implode(', ', $causes) . ")";
            }

            $results['finding'][] = $fault;
        }

        if($results['finding']) {
            $results['finding'] = implode(' ', $results['finding']);
        }
        else {
            $results['finding'] = 'all ok';
        }

        foreach($values as $key => $value) {
            if(array_key_exists($key, $expected) && is_callable($fn = $expected[$key])) {
                $result = $fn($value);

                $new_value = $value;
                if(is_bool($value)) {
                    $new_value = $value === true ? 'yes' : 'no';
                }
                elseif(is_null($value) || $value === '-') {
                    $new_value = '<null>';
                }

                if(! $styled) {
                    $results[(! $result ? '* ' : '') . $key] = $new_value;
                }
                else {
                    $results["<fg=".(! $result ? 'red' : 'white').">{$key} ".(! $result ? '*' : '')."</>"] =
                        "<fg=".(! $result ? 'red' : 'green').">{$new_value}</>";
                }
            }
        }

        return $results;
    }

}