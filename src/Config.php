<?php

namespace Symbiont\Syckdev;

use Symbiont\Syckdev\Concerns\{DealsWithPaths, UsesComposer};
use Symbiont\Config\Config as BaseConfig;
use Symbiont\Config\Drivers\FileDriver;
use Symbiont\Config\JsonConfig;

class Config extends JsonConfig {

    use DealsWithPaths,
        UsesComposer;

    const FILE_SYCKDEV_CONFIG = '.syckdev.json';

    const KEY_PACKAGES = 'packages';
    const KEY_PATH = 'path';
    const KEY_ALIAS = 'alias';

    const CLB_PATH_CHANGED = 'path-changed';

    public function __construct(array $options = []) {
        $this->values([
            self::KEY_PATH => null,
            self::KEY_PACKAGES => [],
            self::KEY_ALIAS => []
        ]);

        $options[] = FileDriver::OPTION_ALLOW_WITHOUT_FILE;

        parent::__construct(static::FILE_SYCKDEV_CONFIG, $options);
    }

    public function setPath(string $path): self {
        $this->set(static::KEY_PATH, $path);
        $this->trigger('path-changed');
        return $this;
    }

    public function getPath(): ?string {
        return $this->get(self::KEY_PATH);
    }

    public function hasPath(): bool {
        return $this->get(self::KEY_PATH) !== null;
    }

    public function getPackages(): array {
        return $this->get(self::KEY_PACKAGES);
    }

    public function addPackage(string $package): bool {
        if($this->packageInList($package)) {
            return false;
        }

        $this->add(self::KEY_PACKAGES, $package,
            BaseConfig::ADD_ONLY_ARRAYS|
            BaseConfig::ADD_IF_NOT_EXISTS|
            BaseConfig::ADD_UNIQUE_VALUES);

        return $this->save();
    }

    public function packageInList(string $package): bool {
        return in_array($package, $this->getPackages());
    }

    public function removePackage(string $package): bool {
        $this->remove(self::KEY_PACKAGES, $package);
        return $this->save();
    }

    public function clear(): bool {
        $this->set(self::KEY_PACKAGES, []);
        return $this->save();
    }

    public function getPackageAliases() {
        return $this->get('alias',  []);
    }

    public function hasPackageAlias(string $name): bool {
        return $this->hasKey(static::KEY_ALIAS, $name);
    }

    public function getPackageFromAlias(string $name): string {
        if($this->hasPackageAlias($name)) {
            return $this->get(static::KEY_ALIAS . '->' . $name);
        }

        return $name;
    }

    public function addPackageAlias(string $name, string $alias): bool {
        if($this->hasPackageAlias($name)) {
            $this->unset(static::KEY_ALIAS . '->' . $name);
        }
        $this->add(static::KEY_ALIAS, [
            $name => $alias
        ]);
        return $this->save();
    }

    public function removePackageAlias(string $name): bool {
        if($this->hasPackageAlias($name)) {
            $this->unset(static::KEY_ALIAS .'->' . $name);
            return $this->save();
        }

        return false;
    }

}