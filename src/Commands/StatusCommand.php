<?php

namespace Symbiont\Syckdev\Commands;

use Composer\Package\CompletePackage;
use Symbiont\Syckdev\Concerns\{DealsWithArguments,
    DealsWithPaths,
    UsesComposer,
    UsesManager};
use Symbiont\Dipendency\Pendency;
use Symbiont\Syckdev\Config;
use Symbiont\Syckdev\Manager;
use Symbiont\Syckdev\Package;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;

class StatusCommand extends InitCommand {

    use DealsWithArguments,
        DealsWithPaths,
        UsesManager,
        UsesComposer;

    protected function configure() {
        $this->setCommandName('status');
        $this->addOption(name: 'all', shortcut: 'a', description: 'Display all installed packages able to be symlinked');
    }

    /**
     * THIS METHOD IS AN ABSOLUTE MESS! ACTUALLY THIS ENTIRE CLASS IS!
     *
     * It's a proof on concept and therefor it will get better over time ;)
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int {
        $this->clearTerminal($output);

        $input->setInteractive(true);

        /**
         * @var Manager $manager
         */
        $config = Pendency::get('config');
        $manager = Pendency::get('manager');
        $composer = Pendency::get('composer');

        if($config->getPath() === null) {
            if(parent::execute($input, $output) === static::FAILURE) {
                $output->writeln("<error>Unable to start syckdev</error>");

                return static::FAILURE;
            }
        }

        $packages = $input->getOption('all') ?
            $manager->getAvailablePackages() : $manager->getRequiredPackages();

        $selectables = [];

        $output->writeln('Syckdev ' . $composer->getPackage()->getPrettyVersion());

        $header_table = new Table($output);
        foreach([
            'path' => $config->getPath(),
            'package' => $composer->getPackage()->getName(),
            'mode' => ($input->getOption('all') ? '<comment>all</comment>' : '<info>required</info>')
        ] as $label => $value) {
            $header_table->addRow([$label, $value]);
        }
        $header_table->render();

        $packages_table = $this->buildTable($input, $output, $packages, $manager, $selectables);
        $packages_table->setHeaderTitle($input->getOption('all') ? 'all' : 'required');
        $packages_table->render();

        $main_text = $this->generateTextualOptions($input, $output, $packages, $manager);
        $main_answer = strtolower($this->getHelper('question')->ask($input, $output, new Question($main_text, 'q')));

        if($main_answer === 'q') {
            return static::SUCCESS;
        }

        if($main_answer === 'all' && ! $input->getOption('all')) {
            $input->setOption('all', true);
            return $this->rerun($input, $output);
        }

        if($main_answer === 'req' || $main_answer === 'required') {
            $input->setOption('all', false);
            return $this->rerun($input, $output);
        }

        if(str_starts_with($main_answer, 'alias:')) {
            if(! array_key_exists($main_answer, $selectables)) {
                return static::SUCCESS;
            }

            $package = $selectables[$main_answer];

            $alias_text = "Set alias for package <{$package->getName()}>, ";
            $alias_text .= $package->isAliased() ?
                "current <info>" .$package->getAlias() . "</info> " :
                "e.g. namespace/project/package ";

            $alias_text .= "<info>c</info>(ancel), <info>r</info>(emove): ";
            $alias_answer = strtolower($this->getHelper('question')->ask($input, $output, new Question($alias_text, 'c')));

            if($alias_answer === 'c' || $alias_answer === 'cancel') {
                return $this->rerun($input, $output);
            }

            if($alias_answer === 'r' || $alias_answer === 'remove') {
                if($package->isAliased()) {
                    $this->getConfig()->removePackageAlias($package->getName());
                    $current = $package->getAlias();
                    $output->writeln("Alias <info>{$current}</info> removed from package <{$package->getName()}>");
                }
                return $this->rerun($input, $output);
            }

            $this->getConfig()->addPackageAlias($package->getName(), $alias_answer);
            return $this->rerun($input, $output);
        }

        if(! array_key_exists($main_answer, $selectables)) {
            $output->writeln("<warning>{$main_answer}</warning> not a valid command");

            return $this->rerun($input, $output);
        }

        $selected = $selectables[$main_answer];
        $option_text = sprintf("%s selected, ", $selected->getName());

        try {
            $option = substr($main_answer, 0, 1);

            switch($option) {
                case 'i': $option_text .= "displaying info"; break;
                case 'a': $option_text .= "activating"; break;
                case 'd': $option_text .= "deactivating"; break;
            }

            $this->clearTerminal($output);
            $output->writeln($option_text);

            if($results = $selected->{(['i' => 'analyze', 'a' => 'activate', 'd' => 'deactivate'])[$option]}()) {
                if(is_scalar($results)) {
                    $results = [$results];
                }
                if(is_array($results)) {
                    $table = new Table($output);
                    $table->setHeaderTitle($selected->getName());
                    foreach($results as $label => $value) {
                        $table->addRow(["<fg=green>{$label}</>", $value]);
                    }
                    $table->render();
                }
            }

            if($option === 'i') {
                strtolower($this->getHelper('question')->ask($input, $output, new Question('<enter to continue>', '')));
            }

            return $this->rerun($input, $output);

        } catch(\Exception $e) {
            $output->writeln("<error>failure, exiting</error>");
            var_dump($e->getMessage());
            return static::FAILURE;
        }
    }

    protected function rerun(InputInterface $input, OutputInterface $output) {
        // reset input config
        $input->setInteractive(false);

        return $this->execute($input, $output);
    }

    protected function clearTerminal(OutputInterface $output) {
        $output->write(sprintf("\033\143"));
    }

    protected function buildTable(InputInterface $input, OutputInterface $output, array $packages, Manager $manager, array &$selectables): Table {

        $table = (new Table($output))->setHeaders([
            'nr', 'name', 'local', 'installed', 'status', 'type', 'alias', 'why'
        ]);

        foreach($packages as $index => $package) {
            /**
             * @var Package $package
             */
            $number = $index+1;

            $name = $package->getName();
            $active = $package->isActive();
            $invalid = $package->isInvalid();
            $activateable = $package->activateable();

            $selector = '-';

            if($activateable) {
                if($package->isActive()) {
                    $selector = "d:{$number}";
                }
                elseif($package->isInactive()) {
                    $selector = "a:{$number}";
                }
            }

            if($package->isInvalid()) {
                $selector = "i:{$number}";
            }

            if($selector !== '-') {
                $selectables[$selector] = $package;
                $selectables["alias:{$number}"] = $package;
            }

            $table->addRow([
                // number
                (function() use ($selector, $invalid) {
                    return ! $invalid ?
                        $selector : "<comment>{$selector}</comment>";
                })(),
                // name
                (function() use ($package, $activateable, $name) {
                    if($activateable) {
                        return "<info>{$name}</info>";
                    }

                    return "<comment>{$name}</comment>";
                })(),
                // local
                (function() use ($package) {
                    $version = $package->getLocalInDevelopmentVersion();

                    return $package->isActive() ?
                        "<info>{$version}</info> *" :
                        $version;
                })(),
                // installed
                (function() use ($package) {
                    $version = $package->getComposerInstalledVersion();

                    return $package->isActive() ?
                        $version :
                        "<info>{$version}</info> *";
                })(),
                // status
                (function() use ($package, $activateable, $active) {
                    return $package->getReadableTableStatus();
                })(),
                // type
                (function() use ($package) {
                    return $package->isRequired() ?
                        'required' : 'dependent';
                })(),
                // alias
                (function(Config $config) use ($package) {
                    if($package->isAliased()) {
                        $alias = $package->getAlias();

                        return $package->isAliasValid() ?
                            $alias :
                            "<error>{$alias}</error>";
                    }

                    return '-';
                })($this->getConfig()),
                // why
                (function() use ($manager, $package, $name) {
                    $result = $manager->whyIsPackageInstalled($name);
                    return $result === false ? '' : $result;
                })()
            ]);
        }

        return $table;
    }

    protected function generateTableRow($block, $line, array $values, array $lengths = []) {
        $lines = [];

        foreach($values as $index => $value) {
            if($lengths) {
                foreach([$index, $value] as $length_index => $string) {
                    $lines[] = $string . str_repeat(' ', $lengths[$length_index] - strlen(strip_tags($string)));
                }
            }
            else {
                $lines[] = str_repeat($line, (int) $value);
            }
        }

        return implode("",
            [$block.$line, implode($line.$block.$line, $lines), $line.$block
            ]);
    }

    protected function generateTextualOptions(InputInterface $input, OutputInterface $output, array $packages, Manager $manager): string {
        $is_all = $input->getOption('all');
        $count_available = count($manager->getAvailablePackages());
        $count_required = count($manager->getRequiredPackages());

        // default quit
        $main_text = "\n<info>q</info>(uit))";

        // if there is a difference, set switch for required/all
        if($count_available !== $count_required) {
            $main_text .= $is_all ?
                ", <info>req</info>(uired)" :
                ", <comment>all</comment> (switch to all available packages)";
        }

        if(count($packages) === 0) {
            $no_packages_text = 'No Packages available';
            if(! $input->getOption('all') && $count_available > 0) {
                $no_packages_text .= ', <comment>use `-a` to see all available packages or type `all`</comment>';
            }

            $output->writeln("{$no_packages_text}.");
        }
        else {
            $main_text .= "\n<info>a</info>(ctivate)<info>:</info>{nr}, <info>d</info>(eactivate)<info>:</info>{nr}, <comment>i</comment>(nfo)<comment>:</comment>{nr}";
        }

        $main_text .= "\n<enter option>: ";

        return $main_text;
    }
}