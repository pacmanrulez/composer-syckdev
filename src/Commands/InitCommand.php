<?php

namespace Symbiont\Syckdev\Commands;

use Symbiont\Dipendency\Pendency;
use Symbiont\Syckdev\Composer\SyckdevBaseCommand;
use Symbiont\Syckdev\Concerns\DealsWithPaths;
use Symbiont\Syckdev\Manager;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class InitCommand extends SyckdevBaseCommand {

    use DealsWithPaths;

    protected bool $header = false;

    protected function configure() {
        $this->setCommandName('init');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        $config = Pendency::get('config');

        if(! Manager::isSyckdevConfigured()) {
            if(! $this->header) {
                $output->writeln('+------------------+');
                $output->writeln('<comment>Initializing</comment> <info>Syckdev</info>');
                $output->writeln('====================');

                $this->header = true;
            }

            $path_text = "Set path location of packages in-development \n<info>q</info>(uit)\n: ";
            $path_answer = strtolower($this->getHelper('question')->ask($input, $output, new Question($path_text, 'q')));

            if($path_answer === 'q' || $path_answer === 'quit') {
                return static::FAILURE;
            }

            $path_answer = $this->clearPath($path_answer);

            if(is_dir($path_answer)) {
                $config->setPath($path_answer)->save();
            }
            else {
                $output->writeln("<error>Enter a valid path, path <{$path_answer}> not found</error>");
                return $this->execute($input, $output);
            }
        }

        return static::SUCCESS;
    }

}