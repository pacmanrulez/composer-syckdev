<?php

namespace Symbiont\Syckdev\Tests;

use Symbiont\Syckdev\Config;
use Symbiont\Syckdev\SyckdevContainer;

final class Data {

    const DIR_ROOT = './tests/tmp';
    const DIR_SYMLINK = self::DIR_ROOT . '/symlinks';
    const DIR_VENDOR = self::DIR_ROOT .  '/vendor';

    public static $packages = [
        "some-package/name" => "^1.0",
        "another/package-name" => "^2.0",
        "symlinked/package-name" =>  "^3.0",
        "symlinked/another-name" => "^4.0"
    ];

    public static function setupSyckdevConfig(): Config {
        SyckdevContainer::createConfig(self::DIR_SYMLINK, self::DIR_ROOT);
        return SyckdevContainer::getConfig();
    }

    public static function getConfigFilePath(): string {
        return Data::DIR_ROOT.'/'.Config::FILE_SYCKDEV_CONFIG;
    }

    public static function resetConfig() {
        self::saveConfigWith(self::DIR_SYMLINK . '/', []);
    }

    public static function saveConfigWith(?string $path = null, array $packages = []) {
        $result = file_put_contents(
            static::getConfigFilePath(),
            json_encode([
                'path' => $path ?? static::DIR_SYMLINK,
                'packages' => $packages
            ], Config::JSON_ENCODE_FLAGS)
        );

        static::reloadConfig();

        return $result;
    }

    public static function getPackagesFromConfig(): array {
        $config = json_decode(file_get_contents(static::getConfigFilePath()), true);;
        return $config['packages'] ?? [];
    }

    public static function reloadConfig() {
        SyckdevContainer::getConfig()->reload();
    }
}