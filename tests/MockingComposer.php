<?php

namespace Symbiont\Syckdev\Tests;

use Composer\Composer;
use Composer\Config;

trait MockingComposer {


    /**
     * Mock a composer instance.
     *
     * @param InstallationManager $installationManager The installation manager.
     *
     * @param string              $rootType            The root package type.
     *
     * @return Composer|\PHPUnit_Framework_MockObject_MockObject
     */
    private function mockComposerExpensive()
    {
        $config          = $this->getMockBuilder(Config::class)->getMock();
        $composer        = $this
            ->getMockBuilder(Composer::class)
            ->onlyMethods([
                'getConfig'
            ])
            ->getMock();

        $composer
            ->expects($this->any())
            ->method('getConfig')
            ->willReturn($config);

        $config
            ->expects($this->any())
            ->method('get')
            ->with($this->logicalOr(
                $this->equalTo('vendor-dir')
            ))
            ->willReturnCallback(
                function () {
                    return Data::DIR_ROOT . '/vendor';
                }
            );

        return $composer;
    }

}