<?php

namespace Symbiont\Syckdev\Tests\unit;

use PHPUnit\Framework\TestCase;
use Symbiont\Syckdev\Concerns\DealsWithArguments;
use Symbiont\Syckdev\Tests\Mocker;
use Symbiont\Syckdev\Tests\MockingComposer;

final class DealsWithArgumentsTest extends TestCase {

    use MockingComposer;

    protected $args;

    public function setUp(): void {
        parent::setUp();

        $this->args = new class {
            use DealsWithArguments;
        };
    }

    public function testCleanArgument() {
        $expect = 'somepackage/name';
        $arg = 'name=' . $expect;

        $this->assertSame($expect, $this->args->cleanArgument($arg));

        $expect = 'this-will-return';
        $this->assertSame($expect, $this->args->cleanArgument($expect));
    }

    public function testGetArgument() {
        $expect = 'somepackage/name';
        $arg = 'name';

        $input = Mocker::mockInputInterface();
        $input->setArgument($arg, "{$arg}={$expect}");

        $this->assertSame($expect, $this->args->getArgument($arg, $input));
    }

}