<?php

namespace Symbiont\Syckdev\Tests\unit;

use PHPUnit\Framework\TestCase;
use Symbiont\Syckdev\Concerns\DealsWithComposerJson;
use Symbiont\Syckdev\Tests\Data;

final class DealsWithComposerJsonTest extends TestCase {

    protected $trait;

    public function setUp(): void {
        parent::setUp();

        $this->trait = new class {
            use DealsWithComposerJson;

            protected function getComposerJsonfile(): string {
                return file_get_contents(Data::DIR_ROOT . '/composer-test.json');
            }
        };
    }

    public function testGetRequiredPackages() {
        $packages = $this->trait->getRequiredPackages();

        $this->assertIsArray($packages);
        $this->assertEquals(array_keys(Data::$packages), $packages);
    }

}