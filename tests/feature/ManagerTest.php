<?php

namespace Symbiont\Syckdev\Tests\unit;

use Composer\IO\IOInterface;
use PHPUnit\Framework\TestCase;
use Symbiont\Syckdev\Concerns\UsesConfig;
use Symbiont\Syckdev\Config;
use Symbiont\Syckdev\Manager;
use Symbiont\Syckdev\Tests\Data;
use Symbiont\Syckdev\Tests\Mocker;
use Symfony\Component\Console\Output\ConsoleOutput;

final class ManagerTest extends TestCase {

    protected $manager;

    public function setUp(): void {
        $this->manager = new class extends Manager {

            public function __construct() {
                parent::__construct(Mocker::mockIOInterfaces(), true);
            }

            public function callProtected($name, array $args) {
                if(method_exists($this, $name)) {
                    return call_user_func_array([$this, $name], $args);
                }
                throw new \Exception("Test --- method ".get_class($this)."::{$name} not found");
            }

        };
    }

    public function testGenerateTemporaryPackageName() {
        $generated = $this->manager->callProtected('generateTemporaryPackageName', [
            'namespace/package-name'
        ]);

        $this->assertSame('namespace/.package-name.syckdev', $generated);
    }

}