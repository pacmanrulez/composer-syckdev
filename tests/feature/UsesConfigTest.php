<?php

namespace Symbiont\Syckdev\Tests\unit;

use PHPUnit\Framework\TestCase;
use Symbiont\Syckdev\Concerns\UsesConfig;
use Symbiont\Syckdev\Config;
use Symbiont\Syckdev\Tests\Data;

final class UsesConfigTest extends TestCase {

    protected $trait;
    protected $config;

    public function setUp(): void {
        parent::setUp();

        $this->config = Data::setupSyckdevConfig();

        $this->trait = new class {
            use UsesConfig;
        };
    }

    public function tearDown(): void {
        Data::resetConfig();
    }

    public function testGetConfig() {
        $config = $this->trait->getConfig();

        $this->assertInstanceOf(Config::class, $config);
    }

    public function testGetSymlinkPath() {
        $this->assertEquals(Data::DIR_SYMLINK.'/', $this->trait->getSymlinkPath());
    }

    public function testGetPackages() {
        $packages = [
            'some/package-added'
        ];

        $this->config->add($packages[0]);

        $this->assertEquals($packages, $this->trait->getPackages());
    }

}