<?php

namespace Symbiont\Syckdev\Tests\unit;

use PHPUnit\Framework\TestCase;
use Symbiont\Syckdev\Concerns\DealsWithPaths;

final class DealsWithPathsTest extends TestCase {

    protected $paths;

    public function setUp(): void {
        parent::setUp();

        $this->paths = new class {
            use DealsWithPaths;
        };
    }

    public function testClearPath() {
         $this->assertSame('path/with/right/slash/end', $this->paths->clearPath('path/with/right/slash/end/'));
    }

    public function testFinishPath() {
        $this->assertSame('path/with/right/slash/end/', $this->paths->finishPath('path/with/right/slash/end'));
        $this->assertSame('path/with/right/slash/end/', $this->paths->finishPath('path/with/right/slash/end/'));
    }

    public function testAsPath() {
        $paths = ['this/is', 'some/path'];
        $path_expect = 'this/is/some/path';
        $this->assertSame($path_expect, $this->paths->asPath($paths));

        $paths = ['/this/is/', null,'another/path/'];
        $path_expect = '/this/is/another/path';
        $this->assertSame($path_expect, $this->paths->asPath($paths));
    }

}