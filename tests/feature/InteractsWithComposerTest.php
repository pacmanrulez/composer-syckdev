<?php

namespace Symbiont\Syckdev\Tests\unit;

use Composer\Composer;
use PHPUnit\Framework\TestCase;
use Symbiont\Dipendency\Pendency;
use Symbiont\Syckdev\Concerns\InteractsWithComposer;
use Symbiont\Syckdev\Tests\Data;
use Symbiont\Syckdev\Tests\MockingComposer;

final class InteractsWithComposerTest extends TestCase {

    use MockingComposer;

    protected $trait;

    public function setUp(): void {
        Pendency::bind('composer', $this->mockComposerExpensive());

        $this->trait = new class {
            use InteractsWithComposer;
        };
    }

    public function testComposer() {
        $this->assertInstanceOf(Composer::class, $this->trait->composer());
    }

    public function GetVendorDir() {
        $this->assertEquals(Data::DIR_VENDOR, $this->trait->getVendorDir());
    }

}