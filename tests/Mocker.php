<?php

namespace Symbiont\Syckdev\Tests;

use Composer\Composer;
use Composer\Config;
use Composer\IO\IOInterface;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;

final class Mocker {

    public static function mockComposer() {
        return \Mockery::mock(Composer::class, [
            'getConfig' => \Mockery::mock(Config::class, [
                'get' => './vendor'
            ])
        ]);
    }

    public static function mockIOInterfaces() {
        return \Mockery::mock(IOInterface::class);
    }

    public static function mockInputInterface() {
        return new class implements InputInterface {

            protected array $arguments = [];

            public function getArgument(string $name): mixed {
                return $this->arguments[$name] ?? null;
            }

            public function setArgument(string $name, mixed $value): void {
                $this->arguments[$name] = $value;
            }

            public function getFirstArgument(): ?string { return null; }
            public function hasParameterOption(array|string $values, bool $onlyParams = false): bool { return false; }
            public function getParameterOption(array|string $values, float|array|bool|int|string|null $default = false, bool $onlyParams = false): mixed { return null; }
            public function bind(InputDefinition $definition): void { }
            public function validate(): void { }
            public function getArguments(): array { return []; }
            public function hasArgument(string $name): bool { return true; }
            public function getOptions(): array { return []; }
            public function getOption(string $name): mixed { return null; }
            public function setOption(string $name, mixed $value): void { }
            public function hasOption(string $name): bool { return true; }
            public function isInteractive(): bool { return true; }
            public function setInteractive(bool $interactive): void { }
            public function __toString(): string { return ''; }
        };
    }


}