<?php

namespace Symbiont\Syckdev\Tests\unit;

use PHPUnit\Framework\TestCase;

use Symbiont\Config\Contracts\Configurable;
use Symbiont\Syckdev\Concerns\DealsWithPaths;

use Symbiont\Syckdev\Tests\{
    Data
};
use Symbiont\Syckdev\Config;

final class ConfigTest extends TestCase {

    use DealsWithPaths;

    protected Configurable $config;

    protected array $packages = [
        'example/test-package',
        'another/example-package'
    ];

    protected function setUp(): void {
        $this->config = new Config('./tests/tmp/.syckdev.json');
        // we always start with an empty config
        $this->config->values([]);
        $this->config->save();
    }

    public function testAddPackage() {
        $this->assertEmpty($this->config->values());

        $this->config->addPackage('example/test-package');
        $this->assertEquals(['example/test-package'], $this->config->getPackages());
        $this->config->addPackage('another/test-package');
        $this->assertSame(['example/test-package', 'another/test-package'], $this->config->getPackages());
    }

    public function testAddPackageDuplicate() {
        $this->assertEmpty($this->config->values());

        $this->config->addPackage('example/test-package');
        $this->assertEquals(['example/test-package'], $this->config->getPackages());
        $this->config->addPackage('example/test-package');
        $this->assertEquals(['example/test-package'], $this->config->getPackages());
    }

    public function testRemovePackage() {
        $this->assertEmpty($this->config->values());

        $this->config->addPackage('example/test-package');
        $this->assertEquals(['example/test-package'], $this->config->getPackages());
        $this->config->addPackage('another/test-package');
        $this->config->removePackage('example/test-package');
        $this->assertEquals(['another/test-package'], $this->config->getPackages());
        $this->config->removePackage('another/test-package');
        $this->assertEmpty($this->config->getPackages());
    }

    public function testPath() {
        $this->assertEmpty($this->config->values());
        $path = '/some/path/to/symlinks';

        $this->config->setPath($path);
        $this->assertEquals($path, $this->config->getPath());
    }
}