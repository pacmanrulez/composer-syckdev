<?php

namespace Symbiont\Syckdev\Tests\unit;

use PHPUnit\Framework\TestCase;

use Symbiont\Config\Drivers\FileDriver;
use Symbiont\Dipendency\Pendency;
use Symbiont\Syckdev\Concerns\DealsWithPaths;
use Symbiont\Syckdev\Config;
use Symbiont\Syckdev\SyckdevContainer;
use Symbiont\Syckdev\SyckdevPlugin;
use Symbiont\Syckdev\Manager;

use Symbiont\Syckdev\Tests\{
    Data, Mocker
};

final class PluginCreationTest extends TestCase {

    use DealsWithPaths;

    public function testPluginCreation() {
        // make config immutable
        Pendency::bind('config', function() {
            return new Config('./tests/tmp/.syckdev.json');
        }, true);

        $plugin = new SyckdevPlugin();
        $plugin->activate(Mocker::mockComposer(), Mocker::mockIOInterfaces());

        $this->assertNotNull(Pendency::get('composer'));
        $this->assertSame(get_class(Pendency::get('config')), Config::class);
        $this->assertSame(get_class(Pendency::get('manager')), Manager::class);
    }

}