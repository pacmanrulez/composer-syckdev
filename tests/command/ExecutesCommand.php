<?php

namespace Symbiont\Syckdev\Tests\command;

interface ExecutesCommand {

    public function test($input, $output);

}