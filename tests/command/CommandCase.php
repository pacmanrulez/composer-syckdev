<?php

namespace Symbiont\Syckdev\Tests\command;

use Composer\Composer;
use PHPUnit\Framework\TestCase;
use Symbiont\Syckdev\SyckdevPlugin;
use Symbiont\Syckdev\Tests\Data;
use Symbiont\Syckdev\Tests\Mocker;
use Symfony\Component\Console\Output\OutputInterface;

abstract class CommandCase extends TestCase {

    abstract public function testExecute();
    abstract protected function createCommandObject();

    public function setUp(): void {
        SyckdevPlugin::$file_root = Data::DIR_ROOT;
        $plugin = new SyckdevPlugin;
        $plugin->activate($this->getMockBuilder(Composer::class)->getMock(), Mocker::mockIOInterfaces());
    }

    protected function mockOutputInterface() {
        return $this->getMockBuilder(OutputInterface::class)
            ->onlyMethods(['writeln', 'write', 'setVerbosity', 'getVerbosity', 'isQuiet', 'isVerbose', 'isVeryVerbose', 'isDebug', 'setDecorated', 'isDecorated', 'setFormatter', 'getFormatter'])
            ->getMock();
    }

    protected function mockInputInterace(?string $name = null, mixed $value = null) {
        $input = Mocker::mockInputInterface();
        if($name && $value) {
            $input->setArgument($name, "{$name}={$value}");
        }
        return $input;
    }

    /**
     * Run command
     * @param array $packages
     * @return void
     */
    protected function runCommand($input = null, $output = null) {
        $cmd = $this->createCommandObject();
        $input = $input ?? $this->mockInputInterace();
        $output = $output ?? $this->mockOutputInterface();
        $cmd->test($input, $output);
    }

}
