<?php

namespace Symbiont\Syckdev\Tests\command;

use Composer\Command\BaseCommand;

trait HandlesCommandExecution {

    public function test($input, $output) {
        if(! $this instanceof BaseCommand) {
            throw new \Exception(__CLASS__. ' does not implement ' . BaseCommand::class);
        }

        return $this->execute($input, $output);
    }

}