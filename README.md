# Symbiont/Syckdev

This package is work in progress!

## Requirements

- `php 8.2`
- `composer/composer 2.1`

## Installation

```bash
composer require symbiont/composer-syckdev
```

## Documentation

This documentation only documents the technical usage of this package with little to no text.

- Documentation [https://pacmanrulez.gitlab.io/symbiont-syckdev](https://pacmanrulez.gitlab.io/symbiont-syckdev)
- Gitlab [https://gitlab.com/pacmanrulez/symbiont-syckdev](https://gitlab.com/pacmanrulez/symbiont-syckdev)

## Tests

```bash
composer test
```

---

## License

[MIT license](LICENSE.MD)