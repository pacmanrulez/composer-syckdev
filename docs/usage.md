# Usage

This plugin allows the use of custom commands to regulate packages **in-development**, flipping symlinks and original
install directories, to avoid spoiling the `composer.lock` file with **dev** builds.

It generalizes around the idea that all packages **in-development** are located in one single directory using the 
`namespace/package-name` directory structure, e,g, `/var/www-symlinks` would look like:

```bash
- var
  - www-symlinks
    - symbiont
        config
        dispatcher
        dipendency
    - illuminate
        support 
    - whatever
        package-in-development
```



The directory `config` (or the namespace `symbiont`) would be a symlink to the real projects directory, e.g: <br />
`/var/www-symlinks/symbiont/config -> /Users/{user}/projects/symbiont/config`<br /> or something like <br />
`/var/www-symlinks/symbiont -> /Users/{user}/project/symbiont`


The only important rule that matters is that the `path` (e.g. `/var/www-symlinks`) should be a directory based on a structure 
of `namespace/package-name`. It would be wise to use symlinks in general, when for example homestead or any virtual machine is used for development,
the symlinks will work on the host as on any guest (where I heard the rumour, Windows supports symlinks too, but who uses
Windows anyway .. right?).

## Config

Config is located at the root of a given project `.syckdev.json`.

```json
{
  "path": "/path/to/packages/in-development",
  "packages": [
    "<namespace>/<package-name>"
  ],
  "alias": {
    "<namespace>/<package-name>" : "<namespace>/project/<package-name>"
  }
}
```

### `path: <string>`

The path where all packages are located to be symlinked, e.g. `/var/www-symlinks`.

```json
{
  "path": "/var/www-symlinks"
}
```

### `packages: <array>`

The packages activated in a project (symlinked).

```json
{
  "packages": [
    "namespace/package-name"
  ]
}
```

### `alias: <array>`

If packages are divided in projects, e.g. `namespace/project/package`, the composer package name might differentiate from 
the actual directory structure, e.g. `symbiont/support-boot-trait` points to `symbiont/support/boot-trait`. An alias
can control the actual directory compared to its composer package name.

```json
{
  "alias": {
    "namespace/package-name": "actual/namespace/package-name"
  }
}
```

## Commmands

!!! Note
    For now, the only command controlling this plugin is `sd:status`. 

!!! Note
    In general, all commands are callable using the Syckdev namespace `sd`, e.g. using `sd:status`

| Command                           |  Args  | Description                                                |
|:----------------------------------|:------:|:-----------------------------------------------------------|
| [status](#using-the-status-table) |        | Status overview of all packages selectable to be symlinked |

### Using status for the first time

If no `.syckdev.json` config file is present, the `InitCommand` will initialize a default config asking
for the symlink path to the packages **in-development**, the default is `/var/www-symlinks`

```bash
+------------------+
Initializing Syckdev
====================
Set path location of packages in-development 
q(uit)
<path /var/www-symlinks> : 
```

### Using the status table

Table overview for designated `required` packages, using the option `--all` (or `-a`) will show all available packages (also those
required by other packages) which can be symlinks to your package **in-development** (e.g. located in `/var/www-symlinks`).

```shell
composer sd:status --all
```

For example, executing `composer sd:status --all` in the `symbiont/composer-syckdev` package will display the following 
table including options.

<a id="table-1"></a>

```bash title="Table-1"
mode: required
+-----+---------------------------+----------+-----------+------------+-----------+-------+-----------------------------------+
| nr  | name                      | local    | installed | status     | type      | alias | why                               |
+-----+---------------------------+----------+-----------+------------+-----------+-------+-----------------------------------+
| d:1 | symbiont/config           | v1.2.8 * | v1.2.7    | + active   | required  |   -   | symbiont/composer-syckdev (1.0.0) |
| a:2 | symbiont/dipendency       | v1.0.1   | v1.0.1 *  | - inactive | required  |   -   | symbiont/composer-syckdev (1.0.0) |
| a:3 | symbiont/dispatcher       | v1.0.2   | v1.0.2 *  | - inactive | dependent |   -   | symbiont/config (v1.2.7)          |
+-----+---------------------------+----------+-----------+------------+-----------+-------+-----------------------------------+

q(uit)), all (switch to all available packages)
a(ctivate):{nr}, d(eactivate):{nr}
: <enter option>

```

| table header label | description                                                                                                     |
|--------------------|-----------------------------------------------------------------------------------------------------------------|
| nr                 | Package number                                                                                                  |
| name               | Package name `<namespace>/<package-name>`                                                                       |
| installed          | Shows current composer installed global version.                                                                |
| local              | Shows current **in-development** local version.                                                                 |
| status             | Status of a package                                                                                             |
| status: active     | Status `+ active`: Package is activated, symlinked package from `<path>`                                        |
| status: inactive   | Status `- inactive`: Package is deactivated, default installed package by composer                              |
| status: invalid    | Status `? invalid`: Package is installed and found in `<path>` but does not seem to be a valid composer Package |
| status: unknown    | Status `! unknown`: Package status cannot be determined                                                         |
| type               | If package is `required` (designated as `required` in `composer.json`) or `dependent` by another package        |
| why                | Who requires the package                                                                                        |

## Status options

| action | description                                                                              |
|--------|------------------------------------------------------------------------------------------|
| q      | Quit plugin                                                                              |
| all    | Switch to all available packages if in `required` mode                                   |
| req    | Switch to required packages if in `all` mode                                             |
| a:{nr} | Activate package given by number, e.g. `a:1` activates Package 1 (`symbiont/config`)     |
| d:{nr} | Deactivate package given by number, e.g. `d:1` deactivates Package 1 (`symbiont/config`) |
| i:{nr} | Information about the status of the package, e.g `i:1`.                                  |
 
## Textual explanation of Table-1

Textual explanation of [table-1](#table-1)

- `composer sd:status` was executed in a package called `symbiont/composer-syckdev`
- `symbiont/composer-syckdev` requires 2 packages, `symbiont/config` and `symbiont/dipendency`
    - The package `symbiont/config` was activated `+ active` with option `a:1` and can be disabled using option `d:1`. The composer installed version is `v1.2.7` whereas the local symlinked version is at `v1.2.8`
    - The package `symbiont/dipendency` is deactivated `- inactive` and can be activated using the option `a:2` It uses the composer installed version `v1.0.1`
- The package `symbiont/dispatcher` is required by the package `symbiont/config` and is deactivated
    - It is required by `symbiont/config` and uses the composer installed version of `v1.0.2`
- Using `composer sd:status` without `--all` (or `-a`) the package `symbiont/dispatcher` would not show

## Activating/Deactivating a package

A package can only be activated if it is deactivated (and vice versa). If package 1 is deactivated, it can be
activated using `a:1` and once activated, deactivated with `d:1`.

```bash


terminal>: composer sd:status
+-----+-----------------+----------+-----------+------------+----------+-------+-----------------------------------+
| nr  | name            | local    | installed | status     | type     | alias | why                               |
+-----+-----------------+----------+-----------+------------+----------+-------+-----------------------------------+
| a:1 | symbiont/config | v1.2.8   | v1.2.7 *  | - inactive | required |   -   | symbiont/composer-syckdev (1.0.0) |
+-----+-----------------+----------+-----------+------------+----------+-------+-----------------------------------+

// Package `symbiont/config` is deactivated and currently using 
// installed version v1.2.8.

terminal>: a:1
+-----+-----------------+----------+-----------+------------+----------+-------+-----------------------------------+
| nr  | name            | local    | installed | status     | type     | alias | why                               |
+-----+-----------------+----------+-----------+------------+----------+-------+-----------------------------------+
| d:1 | symbiont/config | v1.2.8 * | v1.2.7    | + active   | required |   -   | symbiont/composer-syckdev (1.0.0) |
+-----+-----------------+----------|-----------+------------+----------+-------+-----------------------------------+

// Package `symbiont/config` is activated and currently using a local 
// version of v1.2.8, whereas the installed version is untouched.

terminal>: d:1

// Package `symbiont/config` will be deactivated, etc.
```

When a package (in this example `symbiont/config`) is activated, the composer install directory in `./vendor/symbiont/config` 
will be renamed from `config` to `.config.syckdev` and a symlink will replace the original `config` directory using
`config -> /var/www-symlinks/symbiont/config`. When deactivated, the symlink will be removed and the `.config.syckdev`
will be restored to `config`.

## Package alias

Package paths now often are divided into projects. e.g. the package `symbiont/support-boot-trait` is part of a project
support with a path of `symbiont/support/boot-trait`. In a development environment, this would be most likely the
case and therefor the package can't be found by its name `support-boot-trait`. For such circumstances, an alias can be
given to any package.

!!! Note
    This process is not yet automated, manual edit of the `.syckdev.json` is required.

!!! Example
    The package `symbiont/support-boot-trait` is located in a subproject of `symbiont` called `support`. The path to
    each package is `symbiont/support/*` instead of `symbiont/*`. The `alias` object contains a sequence of
    <br /><br />
      - key: `<package-name>`, e.g. `symbiont/support-boot-trait`<br />
      - value: `<package-path>`, e.g. `symbiont/support/boot-trait`
 
```bash
{
  "path": "/var/www-symlinks",
  "packages": []
  "alias": {
    "symbiont/support-boot-trait": "symbiont/support/boot-trait"
  }
}
```

Now using `composer sd:status` the `symbiont/support-boot-trait` will be included list.

```bash title="Table-2"
terminal>: composer sd:status
+-----+-----------------------------+---------+------------+------------+----------+-----------------------------+-----------------------------------+
| nr  | name                        | local   | installed  | status     | type     | alias                       | why                               |
+-----+-----------------------------+---------+------------+------------+----------+-----------------------------+-----------------------------------+
| a:1 | symbiont/support-boot-trait | v1.2.7  | v1.2.7 *   | - inactive | required | symbiont/support/boot-trait | symbiont/composer-syckdev (1.0.0) |
+-----+-----------------------------+---------+------------+------------+----------+-----------------------------+-----------------------------------+
```

## Composer `install` and `update`

If a `composer update` occurs, all packages that are activated and updated by composer will be reverted
before composer applies the update.

This process is quite complex and will take some time to develop a satisfying result. This applies to `composer update` 
as well as `composer install`.