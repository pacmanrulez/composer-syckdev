document.addEventListener("DOMContentLoaded", function(event) {

    // code block
    let codes = document.querySelectorAll('code.md-code__content');
    codes.forEach(function(code) {
        /**
         * Hide `<?php` from php code blocks.
         */

        let first = code.querySelector('span > span.cp');
        if(first && first.innerText === '<?php') {
            first.parentNode.classList.add('hidden');
        }
    });

    document.querySelectorAll('code.md-code__content span > span.o')
        .forEach(function(span) {
            switch(span.innerText) {
                case '-':
                    span.classList.add('gd');
                    span.parentNode.classList.add('gd');
                    break;
                case '+':
                    span.classList.add('gi');
                    span.parentNode.classList.add('gi');
                    break;
            }
        });

});