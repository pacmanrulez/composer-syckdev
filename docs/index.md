# Syckdev

!!! warning
    This package is `work in progress`!

!!! Note
    This package is tested for Linux/MacOS. Windows has **not** been tested.

!!! Note
    This documentation only documents the technical usage of this package with little to no text.

A composer plugin to ease the headache of developing composer packages locally without spoiling the `composer.lock` file.

## Installation

Requires at least PHP `8.2` and Composer `2.1`

Recommended is a global installation using

```bash
composer global require symbiont/composer-syckdev
```

Or locally

```bash
composer require symbiont/composer-syckdev
```

Allow the plugin when asked after install with `y`

```bash
Do you trust "symbiont/composer-syckdev" to execute code and wish to enable it now? (writes "allow-plugins" to composer.json) [y,n,d,?] y
```

Or manually modify the `config` section in the `composer.json` file located in the composer project. Or globally using:

```bash
// find global composer directory
terminal>: nano $(composer -n config --global home)/composer.json
```

```json
{
  "config": {
    "allow-plugins": {
      "symbiont/composer-syckdev": true
    }
  }
}
```

---

## Testing

```php
// to be written
composer test
```

